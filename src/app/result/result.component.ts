import { Component, OnInit } from '@angular/core';
import { User } from '../app.model';
import { ResultService } from './result.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  result: number;
  
  constructor(
    private resultService : ResultService,
    private router: Router
  ) { }

  ngOnInit() {
    this.result = this.resultService.getResult();
  }

  return()
  {
    this.router.navigateByUrl('/home');
  }

}
