import {  User } from '../app.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})

export class ResultService{
    requestUrl = 'https://ian128.pythonanywhere.com/calculate';
    constructor(private httpClient: HttpClient){}

    getDetailData(user: User): Observable<any>{
        const reqHeader = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(
            this.requestUrl,
            user,
            {headers: reqHeader}
        )
    }

    result : User;

    setResult(res : User)
    {
        // this.result = res;
        
        // console.log(this.result)

        localStorage.setItem("result", JSON.stringify(res));
        console.log(res)
    }

    getResult()
    {
        // return this.result;
        var res = JSON.parse(localStorage.getItem("result"));
        return res.result;
    }
}