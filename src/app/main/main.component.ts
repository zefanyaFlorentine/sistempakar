import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from '../app.model';

import { ResultService } from '../result/result.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  form: FormGroup;
  data : User;

  constructor(
    // private userDataService : UserDataService,
    private resultService : ResultService,
    private router : Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      sikatgigi: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      manis: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      minuman: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      rokok: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      lamasikat: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      periksa: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      })
    })
  }

  onSubmit()
  {
    console.log("INI submit form => ",this.form.value);
    this.data = this.form.value;
    console.log("tampung hasil form ke data => ", this.data)
    this.resultService.getDetailData(this.data).subscribe((res) => {
      this.resultService.setResult(res);
      this.router.navigateByUrl('/result');
    })

  }

}
